# Christoph's dotfiles for server

## Get started

```bash
wget -O - chr1shaefn3r.me/dotfiles-server/setup.sh | bash
```

## Inspiration
After reading through http://dotfiles.github.io and many of the linked projects I ended up at https://github.com/mathiasbynens/dotfiles.
So this is mostly a personalized version of his work plus some additionally features.

## Features
### Zero Prerequisites
What was important to me is, that I have a setup-script which starts with a naked "Linux", instead of having implicit prerequisites like git.
So I added a setup.sh-script which starts by installing all my favourite packages and then sets up all the dotfiles.
The setup.sh-script can be used with a fresh Linux installation, as it only needs wget and bash.
### Host specific settings
I still wanted to be able to have specific setups for every machine in use.
The current setup is to use branches, named after the hostnames they are used on, to create host-optimized setups.
### Automatic update
Additionally I added an automatic update feature to .bashrc.
Now every change I make is automatically incorporated to all the cloned instances (pull-based on every new terminal startup).

