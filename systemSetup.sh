#!/usr/bin/env bash

# Logging stuff
function m_header() { echo -e "\n\033[1m$@\033[0m"; }
function m_success() { echo -e " \033[1;32m✔\033[0m $@"; }
function m_error() { echo -e " \033[1;31m✖\033[0m $@"; }
function m_arrow() { echo -e " \033[1;33m➜\033[0m $@"; }

m_header "SSH Keygen"
ssh-keygen -b 4096 -f ~/.ssh/id_rsa -N ""
m_success "Generated ssh-key to ~/.ssh/id_rsa"

m_header "SSHD hardening"
cp /etc/ssh/sshd_config /etc/ssh/sshd_config.bak
echo "# Hardening" | tee -a /etc/ssh/sshd_config
echo "Protocol 2" | tee -a /etc/ssh/sshd_config
echo "X11Forwarding no" | tee -a /etc/ssh/sshd_config
echo "IgnoreRhosts yes" | tee -a /etc/ssh/sshd_config
echo "MaxAuthTries 2" | tee -a /etc/ssh/sshd_config
m_success "Hardened /etc/ssh/sshd_config"
# Tipps from here: https://linux-audit.com/audit-and-harden-your-ssh-configuration/#securing-the-ssh-server-configuration

################################################################################
# Hook 'Post-systemSetup'
################################################################################
################################################################################

