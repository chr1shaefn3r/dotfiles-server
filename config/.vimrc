if has("autocmd")
	"Remember the positions in files with some git-specific exceptions"
	autocmd BufReadPost *
	 \ if line("'\"") > 0 && line("'\"") <= line("$")
	 \           && expand("%") !~ "COMMIT_EDITMSG"
	 \           && expand("%") !~ "ADD_EDIT.patch"
	 \           && expand("%") !~ "addp-hunk-edit.diff"
	 \           && expand("%") !~ "git-rebase-todo" |
	 \   exe "normal g`\"" |
	 \ endif

	autocmd BufNewFile,BufRead *.patch set filetype=diff
	autocmd BufNewFile,BufRead *.diff set filetype=diff
endif

set smartcase

set background=dark
set t_Co=256

" Make Vim more useful
set nocompatible
" Enhance command-line completion
set wildmenu
" Optimize for fast terminal connections
set ttyfast
" Add the g flag to search/replace by default
set gdefault
" Change mapleader
let mapleader="\<Space>"

" Respect modeline in files
set modeline
set modelines=4
" Enable per-directory .vimrc files and disable unsafe commands in them
set exrc
set secure
" Enable line numbers
set number
" Enable syntax highlighting
syntax on
" Stop syntax highlighting on very long lines
set synmaxcol=500

" Highlight current line
set cursorline
" Default Colors for CursorLine
highlight  CursorLine ctermbg=None ctermfg=None

" Make tabs as wide as two spaces
set tabstop=4
set shiftwidth=4
" Highlight searches
set hlsearch
" Ignore case of searches
set ignorecase
" Highlight dynamically as pattern is typed
set incsearch
" Always show status line
set laststatus=2
" Disable error bells
set noerrorbells
" Don’t reset cursor to start of line when moving around.
set nostartofline
" Show the cursor position
set ruler
" Don’t show the intro message when starting Vim
set shortmess=atI
" Show the current mode
set showmode
" Show the filename in the window titlebar
set title
" Show the (partial) command as it’s being typed
set showcmd
" Start scrolling three lines before the horizontal window border
set scrolloff=3
" Prevents inserting two spaces after punctuation on a join
set nojoinspaces
" Useful so auto-indenting doesn't mess up code when pasting
set pastetoggle=<F9>
" Draw a visual line down the 80th column
set colorcolumn=80
if &columns < 88
	" If we can't fit at least 80-cols, don't display these screen hogs
	set nonumber
	set foldcolumn=0
endif

"" Disable automatic X11 Integration
" Disable mouse control for console Vim (very annoying)
set mouse=
" Disable automatic X11 clipboard crossover
set clipboard=

" Automatic commands
if has("autocmd")
	" Enable file type detection
	filetype on
	filetype plugin on
	" Treat .json files as .js
	autocmd BufNewFile,BufRead *.json setfiletype json syntax=javascript
	" Treat .md files as Markdown
	autocmd BufNewFile,BufRead *.md setlocal filetype=markdown
endif

" Smoothe search-and-replace:
"  * search thing with the usual way: /something
"  * hit cs, replace first match, and hit <Esc>
"  * hit n.n.n.n.n. reviewing and replacing all matches
vnoremap <silent> s //e<C-r>=&selection=='exclusive'?'+1':''<CR><CR>
    \:<C-u>call histdel('search',-1)<Bar>let @/=histget('search',-1)<CR>gv
omap s :normal vs<CR>

" Autowrite on multiple events
" BufLeave = before leaving to another buffer
" InsertLeave = when leaving Insert mode
" http://vimdoc.sourceforge.net/htmldoc/autocmd.html
" autocmd BufLeave,InsertLeave * silent! wall
nnoremap <Leader>w :wall<CR>
nnoremap <Leader>q :wqall<CR>

" keys for faster tab navigation
nmap <C-n> gt
nmap <C-p> gT

" vp doesn't replace paste buffer
function! RestoreRegister()
let @" = s:restore_reg
 return ''
endfunction
function! s:Repl()
 let s:restore_reg = @"
 return "p@=RestoreRegister()\<cr>"
endfunction
vmap <silent> <expr> p <sid>Repl()

filetype off " required
" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
let s:bootstrap = 0
try
	call vundle#begin()
catch /E117/
	let s:bootstrap = 1
	silent !mkdir -p ~/.vim/bundle
	silent !git clone https://github.com/gmarik/Vundle.vim.git ~/.vim/bundle/Vundle.vim
	redraw!
	call vundle#begin()
endtry

" let Vundle manage Vundle, required
Plugin 'gmarik/Vundle.vim'

" Airline
Plugin 'bling/vim-airline'

" Git integration
Plugin 'tpope/vim-fugitive.git'
" enable/disable fugitive/lawrencium integration
let g:airline#extensions#branch#enabled = 1
" Markdown
Plugin 'tpope/vim-markdown.git'

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Hook 'vim-plugins'
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" All of your Plugins must be added before the following line
call vundle#end()
if s:bootstrap
	silent PluginInstall
	quit
end
filetype plugin indent on " required

