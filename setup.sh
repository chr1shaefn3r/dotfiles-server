#!/usr/bin/env bash

# Logging stuff.
function m_header() { echo -e "\n\033[1m$@\033[0m"; }
function m_error() { echo -e " \033[1;31m✖\033[0m $@"; }
function m_arrow() { echo -e " \033[1;33m➜\033[0m $@"; }

# Exit after first error
set -e

################################################################################
# Hook 'add-repositories'
################################################################################
################################################################################
apt-get update
apt-get install -y git git-svn vim screen sl htop bash-completion suckless-tools mmv tree net-tools \
	exfat-fuse \
	pmount \
	sshfs \
	apt-transport-https \
	lsb-release \
	curl \
	imagemagick \
	silversearcher-ag \
	ntfs-3g \
	rsync

cd ~;

HOSTNAME=`hostname`
REPO="https://gitlab.com/chr1shaefn3r/dotfiles-server.git"
CLONE_DESTINATION=".dotfiles"

m_header "Environment"
m_arrow "Working dir: `pwd`"
m_arrow "hostname: $HOSTNAME"

if [ ! -d ".dotfiles" ]; then
	m_header "Clone git repository 'dotfiles' with branch '$HOSTNAME' to `pwd`/$CLONE_DESTINATION"
	git clone -b $HOSTNAME --single-branch $REPO $CLONE_DESTINATION
	if [ $? -ne 0 ]; then
		m_error "Failed to clone branch '$HOSTNAME'"
		m_arrow "Going for master ..."
		m_header "Clone git repository 'dotfiles' to `pwd`/$CLONE_DESTINATION"
		git clone $REPO $CLONE_DESTINATION
		if [ $? -ne 0 ]; then
			m_error "Could not clone git-repo 'dotfiles'"
			m_arrow "Make sure ssh-key setup is working!"
			exit 1;
		fi
	fi
else
	m_header "Update git repository ~/.dotfiles"
	(cd .dotfiles/ && git fetch origin && git reset --hard FETCH_HEAD && git clean -df)
fi

m_header "Starting bootstrap.sh"
(cd .dotfiles/ && source bootstrap.sh)

